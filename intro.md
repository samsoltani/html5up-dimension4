---
title: intro
image: pic01.jpg
---
Hi! I'm Sam and I am a Java Developer.As a portfolio I've put together some awesome projects and applications which you can check out here: <a href="#work">awesome work</a>.

My interest is mostly developing web applications, leveraging the power of java language and Spring framework and also Angular for the frontend.